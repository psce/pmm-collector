#!/bin/bash

ENV_FILE=.envfile
PMM_MARK=.pmm-mark
PROXY_MARK=.proxy-mark

[ -s $ENV_FILE ] && . $ENV_FILE

prepare_env () {
    read -p "Username: " PUSER
    read -p "Password: " PPASS
    read -p "Node exporter <yes/no>: " NODE_EXPORTER
    read -p "Mysql metrics exporter <yes/no>: " MYSQL_METRICS_EXPORTER
    read -p "Mysql queries exporter <yes/no>: " MYSQL_QUERIES_EXPORTER
    read -p "Remote IP: " REMOTE_SIP
    read -p "Container: " CONTAINER
    echo "USER=$PUSER"            > $ENV_FILE
    echo "PASS=$PPASS"   >> $ENV_FILE
    echo "NODE_EXPORTER=$NODE_EXPORTER"     >> $ENV_FILE
    echo "MYSQL_METRICS_EXPORTER=$MYSQL_METRICS_EXPORTER"   >> $ENV_FILE
    echo "MYSQL_QUERIES_EXPLORER=$MYSQL_QUERIES_EXPORTER"   >> $ENV_FILE
    echo "REMOTE_SIP=$REMOTE_SIP" >> $ENV_FILE
    echo "CONTAINER=$CONTAINER" >> $ENV_FILE
    }

usage () {
    echo "Usage: $0 <setup|start|stop|destroy>"
    exit 1
}

tunnel80_start () {
    tunnel80_stop &>/dev/null
    sleep 1
    screen -S tunnel80 -d -m  \
                        /bin/bash -c  "LD_LIBRARY_PATH=$LD_LIBRARY_PATH:libs /usr/bin/ssh -N  \
                                -i id_rsa \
                                -o 'StrictHostKeyChecking no' \
                                -o ProxyCommand='./proxytunnel \
                                -P $PUSER:$PPASS \
                                -q -E -p $CONTAINER:443 \
                                -d $REMOTE_SIP:222' \
                                -L 90:127.0.0.1:80 proxy@t80"
}

tunnel42000_start () {
    tunnel42000_stop &>/dev/null
    sleep 1
    screen -S tunnel42000 -d -m \
                        /bin/bash -c "LD_LIBRARY_PATH=$LD_LIBRARY_PATH:libs /usr/bin/ssh -N  \
                                -i id_rsa \
                                -o 'StrictHostKeyChecking no' \
                                -o ProxyCommand='./proxytunnel \
                                -P $PUSER:$PPASS \
                                -q -E -p $CONTAINER:443 \
                                -d $REMOTE_SIP:222' \
                                -R 42000:127.0.0.1:42000 proxy@t42000"

}

tunnel42002_start () {
    tunnel42002_stop &>/dev/null
    sleep 1
       screen -S tunnel42002 -d -m \
                            /bin/bash -c "LD_LIBRARY_PATH=$LD_LIBRARY_PATH:libs /usr/bin/ssh -N  \
                                -i id_rsa \
                                -o 'StrictHostKeyChecking no' \
                                -o ProxyCommand='./proxytunnel \
                                -P $PUSER:$PPASS \
                                -q -E -p $CONTAINER:443 \
                                -d $REMOTE_SIP:222' \
                                -R 42002:127.0.0.1:42002 proxy@t42002"

}

tunnel80_stop () {
    screen -X -S "tunnel80" stuff "^C"
}
tunnel42000_stop () {
    screen -X -S "tunnel42000" stuff "^C"
}
tunnel42002_stop () {
    screen -X -S "tunnel42002" stuff "^C"
}

setup () {
    if [ ! -e id_rsa ]; then
        echo "Private key id_rsa not found!"
        exit 1
    fi
    chmod 600 id_rsa
    prepare_env
    if [ -e /etc/redhat-release ]; then
        echo "RedHat/CentOS..."
        yum list 2>&1| grep pmm-client | grep -w installed &>/dev/null; PMM=$? 
        yum -y install http://www.percona.com/downloads/percona-release/redhat/0.1-6/percona-release-0.1-6.noarch.rpm

        if [ $PMM -ne 0 ]; then
            yum -y install pmm-client
            touch $PMM_MARK
        fi
        #yum list 2>&1| grep proxytunnel | grep -w installed &>/dev/null ; PT=$?
        #if [ $PT -ne 0 ]; then
        #    yum -y install http://ftp.tu-chemnitz.de/pub/linux/dag/redhat/el7/en/x86_64/rpmforge/RPMS/proxytunnel-1.9.0-1.el7.rf.x86_64.rpm
        #    touch $PROXY_MARK
        #fi
    fi

    if [ -e /etc/lsb-release ]; then
        echo "UBUNTU..."
        apt-get update
        apt-get -y install screen
        apt-cache search pmm-client | grep -w pmm-client &>/dev/null; PR=$?
        dpkg -l | grep -w '^ii' | grep -w pmm-client &>/dev/null; PMM=$?

        if [ $PMM -ne 0 ]; then 
            if [ $PR -ne 0 ]; then 
                wget https://repo.percona.com/apt/percona-release_0.1-6.$(lsb_release -sc)_all.deb
                dpkg -i percona-release_0.1-6.$(lsb_release -sc)_all.deb
                rm -f percona-release_0.1-6.$(lsb_release -sc)_all.deb
            fi
            apt-get update
            apt-get -y install pmm-client
            touch $PMM_MARK
        fi
        #dpkg -l | grep -w '^ii' | grep -w proxytunnel &>/dev/null; PT=$?
        #if [ $PT -ne 0 ]; then
        #    apt-get update
        #    apt-get -y install proxytunnel
        #   touch $PROXY_MARK
        #fi     
    fi

    tunnel80_start
    sleep 1
    pmm-admin config --server 127.0.0.1:90 --client-address 127.0.0.1
    
    if [ "x$NODE_EXPORTER" == "xyes" ]; then
        pmm-admin add  linux:metrics
        tunnel42000_start
    fi
    if [ "x$MYSQL_METRICS_EXPORTER" == "xyes" ] || [ "x$MYSQL_QUERIES_EXPORTER" == "xyes" ] ; then
        if [ "x$MYSQL_METRICS_EXPORTER" == "xyes" ]; then
            pmm-admin add mysql:metrics
        fi
    
        if [ "x$MYSQL_QUERIES_EXPORTER" == "xyes" ]; then
            pmm-admin add mysql:queries
        fi
        tunnel42002_start
    fi

}

start () {
    tunnel80_start
    sleep 1

    if [ "x$NODE_EXPORTER" == "xyes" ]; then
        pmm-admin start linux:metrics
        tunnel42000_start
    fi

    if [ "x$MYSQL_METRICS_EXPORTER" == "xyes" ] || [ "x$MYSQL_QUERIES_EXPORTER" == "xyes" ] ; then
        if [ "x$MYSQL_EXPORTER" == "xyes" ]; then
            pmm-admin start mysql:metrics   
        fi
        if [ "x$MYSQL_QUERIES_EXPORTER" == "xyes" ]; then
            pmm-admin start mysql:queries
        fi
        tunnel42002_start
    fi
}

stop () {
    if [ "x$NODE_EXPORTER" == "xyes" ]; then
        tunnel42000_stop
        pmm-admin stop linux:metrics
    fi

    if [ "x$MYSQL_METRICS_EXPORTER" == "xyes" ] || [ "x$MYSQL_QUERIES_EXPORTER" == "xyes" ] ; then
        if [ "x$MYSQL_EXPORTER" == "xyes" ]; then
            pmm-admin stop mysql:metrics   
        fi
        if [ "x$MYSQL_QUERIES_EXPORTER" == "xyes" ]; then
            pmm-admin stop mysql:queries
        fi
        tunnel42002_stop
    fi
    tunnel80_stop
}

destroy () {
    stop
    tunnel80_start
    rm -f $ENV_FILE
    if [ -e $PMM_MARK ]; then
        if [ -e /etc/lsb-release ]; then   
            apt-get -y -f remove pmm-client
        else 
            yum -y remove pmm-client
        fi
        rm -f $PMM_MARK
    fi

    #if [ -e $PROXY_MARK ]; then
    #    if [ -e /etc/lsb-release ]; then   
    #        apt-get -y -f remove proxytunnel
    #    else 
    #        yum -y remove proxytunnel
    #    fi
    #    rm -f $PROXY_MARK        
    #fi
    rm -f $ENV_FILE
    tunnel80_stop
}

case "$1" in
    setup)
        setup
        ;;
    start)
        start
        ;;
    stop)
        stop
        ;;
    destroy)
        destroy
        ;;
    *)
        usage
esac

